
//#include <knx.h>
//#include "Binaereingang.h"
//#include "KnxHelper.h"
#include "hardware.h"

#define SW5V 12

#define CH1 11
#define CH2 10
#define CH3 9
#define CH4 8
#define CH5 7
#define CH6 6
#define CH7 5
#define CH8 4
#define CH9 3
#define CH10 2
#define CH11 1
#define CH12 0

bool LED_Light = false;
bool channel_RP[MAX_NUMBER_OF_GPIO_Channels] = {0};

bool state_RP[MAX_NUMBER_OF_GPIO_Channels] = {1};
bool state_Last_RP[MAX_NUMBER_OF_GPIO_Channels] = {1};
bool state_KO_RP[MAX_NUMBER_OF_GPIO_Channels];
bool state_LED_RP[MAX_NUMBER_OF_GPIO_Channels];
bool state_Lock_RP[MAX_NUMBER_OF_GPIO_Channels] = {0};
bool bus_start_sent_RP[MAX_NUMBER_OF_GPIO_Channels] = {0};

void init_RP_Inputs()
{
    // initialize digital pin LED_BUILTIN as an output.
    pinMode(SW5V, OUTPUT);
    pinMode(CH1, INPUT);
    pinMode(CH2, INPUT);
    pinMode(CH3, INPUT);
    pinMode(CH4, INPUT);
    pinMode(CH5, INPUT);
    pinMode(CH6, INPUT);
    pinMode(CH7, INPUT);
    pinMode(CH8, INPUT);
    pinMode(CH9, INPUT);
    pinMode(CH10, INPUT);
    pinMode(CH11, INPUT);
    pinMode(CH12, INPUT);
    digitalWrite(SW5V, HIGH);
}

void printChannel()
{
    for (int i = 0; i < MAX_NUMBER_OF_GPIO_Channels; i++)
    {
        Serial.print("CH");
        Serial.print(i + 1);
        Serial.print(": ");
        Serial.println(channel_RP[i]);
    }
    Serial.println("-----");
}

void setVCC_GPIO(bool state)
{
   digitalWrite(SW5V, state);
}

void read_RP_GPIOs()
{
    state_RP[0] = digitalRead(CH1);
    state_RP[1] = digitalRead(CH2);
    state_RP[2] = digitalRead(CH3);
    state_RP[3] = digitalRead(CH4);
    state_RP[4] = digitalRead(CH5);
    state_RP[5] = digitalRead(CH6);
    state_RP[6] = digitalRead(CH7);
    state_RP[7] = digitalRead(CH8);
    state_RP[8] = digitalRead(CH9);
    state_RP[9] = digitalRead(CH10);
    state_RP[10] = digitalRead(CH11);
    state_RP[11] = digitalRead(CH12);
}

bool read_GPIO_Inputs(uint8_t ch)
{
    switch (ch)
    {
    case 0 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH1);
        break;
    case 1 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH2);
        break;
    case 2 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH3);
        break;
    case 3 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH4);
        break;
    case 4 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH5);
        break;
    case 5 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH6);
        break;
    case 6 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH7);
        break;
    case 7 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH8);
        break;
    case 8 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH9);
        break;
    case 9 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH10);
        break;
    case 10 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH11);
        break;
    case 11 + MAX_NUMBER_OF_I2C_Channels:
        return digitalRead(CH12);
        break;

    default:
        return 0;
        break;
    }
    return 0;
}

void isChange_RP(uint8_t i)
{
    if (state_RP[i] != state_Last_RP[i] || bus_start_sent_RP[i] == 1)
    {
        bus_start_sent_RP[i] = 0; // Buswiederkehr Wert senden

        if (state_Lock_RP[i] == 0)
        {
            //if (knx.paramByte(getPar(BIN_CHOutputValue, i)))
            if (true)
                state_KO_RP[i] = state_RP[i]; // Wert für 'offen' = 1
            else
                state_KO_RP[i] = !state_RP[i]; // Wert für 'offen' = 0

            SERIAL_PORT.print("State_");
            SERIAL_PORT.print(MAX_NUMBER_OF_GPIO_Channels + i);
            SERIAL_PORT.print(": ");
            SERIAL_PORT.println(state_KO_RP[i]);
            /*
      if (knx.paramByte(getPar(BIN_CHFallWertSenden, i)) && state_KO_RP[i] == LOW || knx.paramByte(getPar(BIN_CHSteigWertSenden, i)) && state_KO_RP[i] == HIGH)
      {
        knx.getGroupObject(getCom(BIN_KoGO_STATE_, i)).value(state_KO_RP[i], getDPT(VAL_DPT_1));
      }*/
            state_Last_RP[i] = state_RP[i];
        }
        else
        {
            //SERIAL_PORT.print("CH");
            //SERIAL_PORT.print(i);
            //SERIAL_PORT.println(": gesperrt");
        }
    }
}