#include <Wire.h>
#include "hardware.h"
#include "Device_setup.h"

void appSetup();
void appLoop();

/*
bool delayCheck(uint32_t iOldTimer, uint32_t iDuration)
{
  return millis() - iOldTimer >= iDuration;
}*/

// the setup function runs once when you press reset or power the board
void setup()
{
  Serial.begin(115200);
  
  // Read HW-ID
  initHW();
  get_HW_ID();
  print_HW_ID();

  pinMode(get_PROG_LED_PIN(), OUTPUT);
  digitalWrite(get_PROG_LED_PIN(), HIGH);
  delay(5000); // ******************  ENTFERNEN   ********************************************
  digitalWrite(get_PROG_LED_PIN(), LOW);
  SERIAL_PORT.println("Startup called...");
  //ArduinoPlatform::SerialDebug = &SERIAL_PORT;

  appSetup();
}

// the loop function runs over and over again forever
void loop()
{

  appLoop();
}
