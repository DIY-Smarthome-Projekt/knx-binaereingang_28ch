//#include <knx.h>
//#include "Binaereingang.h"
//#include "KnxHelper.h"
#include "hardware.h"

#include "PCF8574.h"

//#include "BinaereingangDevice.h"
#include "classReadInput.h"

classReadInput Inputs[MAX_NUMBER_OF_I2C_Channels];

PCF8575 pcf8575_IO(i2cAddr_IO, &Wire1);
PCF8574 pcf8574_VCC(i2cAddr_VCC, &Wire1);

uint16_t state_VCC[MAX_NUMBER_OF_I2C_Channels] = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7};

void init_I2c_Inputs()
{
  for (int i = 0; i < MAX_NUMBER_OF_I2C_Channels; i++)
  {
// read first time all inputs
    //state[i] = pcf8575_IO.pcf8575_Read_NEU(i);                //**********************************************************************
    //state_Last[i] = state[i];                                 //**********************************************************************
  }
  pcf8574_VCC.write8(HIGH);
  SERIAL_PORT.print("PCF: ");
  SERIAL_PORT.println(pcf8574_VCC.isConnected());
}

void setVCC_I2C(bool state, uint8_t ch)
{
    if(state == true)
     pcf8574_VCC.write(state_VCC[ch], LOW);
    else  
     pcf8574_VCC.write(state_VCC[ch], HIGH);
}        

bool read_I2C_Inputs(uint8_t ch)
{
  return pcf8575_IO.pcf8575_Read_NEU(ch);
}

