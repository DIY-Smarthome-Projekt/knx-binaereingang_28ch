#pragma once

//#include <knx.h>
//#include "Binaereingang.h"
//#include "KnxHelper.h"
#include "hardware.h"
#include "classReadInput.h"
#include "Binaereingang_RP2040.h"
#include "Binaereingang_I2C.h"
#include "LED_Statusanzeige.h"

uint32_t ReadInputDelay = 0;
//bool isDelay = true;
bool isReady = true;
bool isReady_RP = true;

uint32_t sendCyclicalDelay[MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels] = {0};
uint8_t CH = 0;

//_____ State maschine EINGAENGE ______
enum State
{
  SetVCC = 1,
  ReadIO = 2,
  ResetVCC = 3,
  SendKO = 4,
  SetLEDS = 5,
  Finish = 10,
} CHState;
//______________________________________

uint8_t channel = 0;
uint8_t channel_oben = 0;
uint8_t function = 1;

bool state[MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels] = {1};
bool state_Last[MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels] = {1};
bool state_KO[MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels];
bool state_Lock[MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels] = {0};
bool bus_start_sent[MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels] = {0};

void init_read_I2C_Inputs()
{
  for (int i = 0; i < MAX_NUMBER_OF_I2C_Channels; i++)
  {
   // read first time all inputs
   state[i] = read_I2C_Inputs(i);                
   state_Last[i] = state[i];                                 
  }
}


void set_state_Lock(uint8_t channel)
{
  state_Lock[channel] = true;
}

void reset_state_Lock(uint8_t channel)
{
  state_Lock[channel] = false;
}

void isChange(uint8_t i)
{
  if (state[i] != state_Last[i] || bus_start_sent[i] == 1)
  {
    bus_start_sent[i] = 0; // Buswiederkehr Wert senden

    if (state_Lock[i] == 0)
    {
      //if (knx.paramByte(getPar(BIN_CHOutputValue, i)))
      if (true)
        state_KO[i] = state[i]; // Wert für 'offen' = 1
      else
        state_KO[i] = !state[i]; // Wert für 'offen' = 0

      SERIAL_PORT.print("State_");
      SERIAL_PORT.print(i+1);
      SERIAL_PORT.print(": ");
      SERIAL_PORT.println(state_KO[i]);
      /*
      if (knx.paramByte(getPar(BIN_CHFallWertSenden, i)) && state_KO[i] == LOW || knx.paramByte(getPar(BIN_CHSteigWertSenden, i)) && state_KO[i] == HIGH)
      {
        knx.getGroupObject(getCom(BIN_KoGO_STATE_, i)).value(state_KO[i], getDPT(VAL_DPT_1));
      }*/
      state_Last[i] = state[i];
    }
    else
    {
      //SERIAL_PORT.print("CH");
      //SERIAL_PORT.print(i);
      //SERIAL_PORT.println(": gesperrt");
    }
  }
}

void ProcessInputs()
{
  //if (delayCheck(ReadInputDelay, knx.paramByte(BIN_InputReadTime) * 100))
  if (delayCheck(ReadInputDelay, 100))
  {
    isReady = true;
    if (channel > MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels)
    {
      channel = 0;
      CHState = SetVCC;
    }
    ReadInputDelay = millis();
  }

  if (isReady)
  {
    if (channel < MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels)
    {
      //Statemaschine Read Inputs
      switch (CHState)
      {
      case SetVCC:
        // Set VCC I2C Inputs
        if (channel <= MAX_NUMBER_OF_I2C_Channels)
        {
          setVCC_I2C(true, channel);
        }
        // Set VCC GPIO Inputs
        else if (channel > MAX_NUMBER_OF_I2C_Channels && channel <= MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels)
        {
          setVCC_GPIO(true);
        }
        CHState = ReadIO;
        break;

      case ReadIO:
        // Read I2C Inputs
        if (channel <= MAX_NUMBER_OF_I2C_Channels)
        {
          state[channel] = read_I2C_Inputs(channel);
        }
        // Read GPIO Inputs
        else if (channel > MAX_NUMBER_OF_I2C_Channels && channel <= MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels)
        {
          state[channel] = read_GPIO_Inputs(channel);
        }
        CHState = ResetVCC;
        break;

      case ResetVCC:
        // ReSet VCC I2C Inputs
        if (channel <= MAX_NUMBER_OF_I2C_Channels)
        {
          //setVCC_I2C(false, channel);
        }
        // ReSet VCC GPIO Inputs
        else if (channel > MAX_NUMBER_OF_I2C_Channels && channel <= MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels)
        {
          //setVCC_GPIO(false);
        }
        CHState = SendKO;
        break;

      case SendKO:
        isChange(channel);
        CHState = SetLEDS;
        break;

      case SetLEDS:
        set_State_LED(channel, state[channel]);
        CHState = Finish;
        break;

      case Finish:
        channel++;
        if (channel == MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels)
        {
          channel = MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels + 1;
          isReady = false;
        }
        CHState = SetVCC;
        break;

      default:
        break;
      }
    }
  }
}
