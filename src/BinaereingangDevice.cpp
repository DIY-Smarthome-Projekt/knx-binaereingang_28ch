#include <Arduino.h>

//#include <knx.h>
//#include "Binaereingang.h"
#include "hardware.h"
//#include "KnxHelper.h"
#include "BinaereingangDevice.h"
#include "BinaereingangHandleCH.h"
#include "Device_setup.h"


uint32_t heartbeatDelay = 0;
uint32_t startupDelay = 0;
uint32_t LED_Delay = 0;
uint32_t READ_Delay = 0;
uint32_t READ_PRINT = 0;

uint32_t TestDelay = 0;
bool TestLEDstate = false;

bool delayCheck(uint32_t iOldTimer, uint32_t iDuration)
{
    return millis() - iOldTimer >= iDuration;
}

void appSetup()
{
  initStatusLED();
  digitalWrite(get_Status_LED_PIN(),HIGH);

  Serial.println("init I2C");
  //I2C Init
  Wire.setSDA(20);
  Wire.setSCL(21);

  Wire1.setSDA(14);
  Wire1.setSCL(15);
  
  Wire.begin();
  Wire1.begin();

  Serial.println("init I2C Inputs");
  init_I2c_Inputs();
  init_read_I2C_Inputs();

  Serial.println("init RP Inputs");
  init_RP_Inputs();

  Serial.println("init Status LEDs");
  initI2cStatusLeds(true,true);
  //pinMode(LED_YELLOW_PIN, OUTPUT);

  // load ETS parameters
  //load_ETS_par();

  //State Maschine init
  CHState = SetVCC;
  Serial.println("Setup Done");
  digitalWrite(get_Status_LED_PIN(),LOW);
}

void appLoop()
{

  ProcessInputs();

  //ProcessSendCyclical();
 
  /*
  if (delayCheck(READ_Delay, 100))
  {
    READ_Delay = millis();
    read_RP_GPIOs();
  }
  
  if (delayCheck(READ_PRINT, 1000))
  {
    READ_PRINT = millis();
    printChannel();
  }
  */

  if (delayCheck(LED_Delay, 500))
  {
    LED_Light = !LED_Light;
    //digitalWrite(LED_YELLOW_PIN, LED_Light);
    LED_Delay = millis();
  }

}