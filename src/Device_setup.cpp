#include "Device_setup.h"
#include <stdint.h>
#include <Arduino.h>
#include "hardware.h"


uint8_t HW_ID = 0;


uint8_t get_PROG_LED_PIN()
{
  switch (HW_ID)
  {
  case HW_1_0: // V1.x
    return 24;
    break;
  default:
    return 255;
    break;
  }
}

uint8_t get_Status_LED_PIN()
{
  switch (HW_ID)
  {
  case HW_1_0: // V1.x
    return 22;
    break;
  default:
    return 255;
    break;
  }
}

uint8_t get_PROG_BUTTON_PIN()
{
  switch (HW_ID)
  {
  case HW_1_0: // V1.x
    return 25;
    break;

  default:
    return 250;
    break;
  }
}

uint8_t get_SAVE_INTERRUPT_PIN()
{
  switch (HW_ID)
  {
  case HW_1_0: // V1.x
      return 23; 
    break;

  default:
    return 252;
    break;
  }
}

void print_HW_ID()
{
    SERIAL_PORT.print("HW-ID: ");
    switch (HW_ID)
    {
    case HW_1_0:
        SERIAL_PORT.println("V1.x");
        break;
    
    default:
        break;
    }
}

uint8_t get_HW_ID()
{ 
  uint8_t hw_ID = 0;
  // read Inputs
  bitWrite(hw_ID, 0, digitalRead(ID1));
  bitWrite(hw_ID, 1, digitalRead(ID2));
  bitWrite(hw_ID, 2, digitalRead(ID3));

  HW_ID = hw_ID;

  return hw_ID;
}

void initStatusLED()
{
   pinMode(get_Status_LED_PIN(), OUTPUT);
}

void initHW()
{
  uint8_t hw_ID = 0;
  // Set Inputs
  pinMode(ID1, INPUT_PULLUP);
  pinMode(ID2, INPUT_PULLUP);
  pinMode(ID3, INPUT_PULLUP);

  // read Inputs
  bitWrite(hw_ID, 0, digitalRead(ID1));
  bitWrite(hw_ID, 1, digitalRead(ID2));
  bitWrite(hw_ID, 2, digitalRead(ID3));
}