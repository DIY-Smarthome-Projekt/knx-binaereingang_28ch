

#include "classReadInput.h"
#include "BinaereingangDevice.h"
#include "hardware.h"
//#include <knx.h>
//#include "Binaereingang.h"

classReadInput::classReadInput()
{
}

classReadInput::~classReadInput()
{
}

bool classReadInput::getState()
{
  return state;
}

bool classReadInput::getSperrobject()
{
  return sperrobject;
}

bool classReadInput::getStateLED()
{
  return stateLED;
}

void classReadInput::setCH(uint8_t channel, uint8_t function)
{
  
}

void classReadInput::readCH(uint8_t channel, uint8_t function)
{
  pinMode(channel, INPUT);
  //readBuffer(i2cAddr_IO);
  state = (bool) (pcf8575_ReadAll(i2cAddr_IO)<<channel);
}

void classReadInput::sendCH(uint8_t channel, uint8_t function)
{
}

void classReadInput::resetCH(uint8_t channel, uint8_t function)
{

}

void classReadInput::setLEDS(uint8_t channel, uint8_t function)
{
  //if(knx.paramByte(BIN_LEDdisplayDirection)==0) // LED leuchtet wenn Input geschlossen  ******************************************Ändern*************************************
  if(function==0)//nur zum Testen
  stateLED = !state;
  else
  stateLED = state;
  // I2C Expander 16Bit (I2C_Addr_LED )
  if (channel < 16)
  {
    pinMode(15-channel, OUTPUT);
    pcf8575_Write(15-channel, stateLED);
  }
}