#pragma once

#include <stdint.h>

#define ID1 13
#define ID2 18
#define ID3 19


#define HW_1_0  7 // ID1 = H ; ID2 = H ; ID3 = H  --> b00000111 --> 0x07 --> dez 7


uint8_t get_HW_ID();
void initHW();
void initStatusLED();

uint8_t get_Status_LED_PIN();
uint8_t get_PROG_LED_PIN();
uint8_t get_PROG_BUTTON_PIN();
void print_HW_ID();
uint8_t get_SAVE_INTERRUPT_PIN();