#include "hardware.h"
#include "PCF8575.h"

#define I2C_Expander_CH 15

#define CH17 16
#define CH18 17
#define CH19 18
#define CH20 19
#define CH21 20
#define CH22 21
#define CH23 22
#define CH24 23
#define CH25 24
#define CH26 25
#define CH27 26
#define CH28 27


PCF8575 pcf8575_LED_CH1_16(i2cAddr_LED_CH1_16, &Wire);
PCF8575 pcf8575_LED_CH17_28(i2cAddr_LED_CH17_28, &Wire);

bool state_LED[MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels];

bool status_Led_ON = false;
uint16_t state_LED_Out = 0xFFFF;
uint16_t state_LED_Out_17_28 = 0xFFFF;

void initI2cStatusLeds(bool Ch1, bool Ch2)
{
    // LED I2C Inputs
    if (CH1)
    {
        for (int i = 0; i < MAX_NUMBER_OF_I2C_Channels; i++)
        {
            pcf8575_LED_CH1_16.pinMode(i, OUTPUT);
        }
        pcf8575_LED_CH1_16.pcf8575_Clear();
    }
    // LED GPIO Inputs
    if (CH2)
    {
        for (int i = 0; i < MAX_NUMBER_OF_GPIO_Channels; i++)
        {
            pcf8575_LED_CH17_28.pinMode(i, OUTPUT);
        }
        pcf8575_LED_CH17_28.pcf8575_Clear();
    }
}

void setLED_ON_CH1_14()
{
    pcf8575_LED_CH1_16.pcf8575_WriteALL(0x0000);
    status_Led_ON = true;
}

void setLED_OFF_CH1_14()
{
    pcf8575_LED_CH1_16.pcf8575_WriteALL(0xFFFF);
    status_Led_ON = false;
}

void setLED_ON_CH15_28()
{
    pcf8575_LED_CH17_28.pcf8575_WriteALL(0x0000);
    status_Led_ON = true;
}

void setLED_OFF_CH15_28()
{
    pcf8575_LED_CH17_28.pcf8575_WriteALL(0xFFFF);
    status_Led_ON = false;
}

void set_State_LED(uint8_t ch, bool state)
{
    //prüfen ob Channel aktiv und LED Funktion an
    //if (knx.paramByte(getPar(BIN_CHFunktion, channel)) == 0 && knx.paramByte(BIN_InactiveCHLED) == 0)
    if (false)
    {
        state_LED[ch] = 1; // Status_LED =  1 -> LED OFF
    }
    else
    {
        //if (knx.paramByte(BIN_LEDdisplayDirection) == 0) // LED leuchtet wenn Input geschlossen
        if (false) // LED leuchtet wenn Input geschlossen
        {
            state_LED[ch] = !state;
        }
        else
        {
            state_LED[ch] = state;
        }
    }

    // Set VCC I2C Inputs
    if (ch <= MAX_NUMBER_OF_I2C_Channels)
    {
        state_LED_Out ^= (-state_LED[I2C_Expander_CH - ch] ^ state_LED_Out) & (1 << ch);
        // Parameter1: Status LED "ON" / "OFF"  ||   Parameter2: Status LEDs KO gesteuert
        //if (knx.paramByte(BIN_LEDdisplay) && knx.paramByte(BIN_LEDdisplayKOActivation) == 0 || status_Led_ON)
        if (true)
        {
            pcf8575_LED_CH1_16.pcf8575_WriteALL(state_LED_Out);
        }
    }
    // Set VCC GPIO Inputs
    else if (ch > MAX_NUMBER_OF_I2C_Channels && ch <= MAX_NUMBER_OF_I2C_Channels + MAX_NUMBER_OF_GPIO_Channels)
    {
        switch (ch)
        {
        case CH17:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (15));
            break;
        case CH18:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (14));
            break; 
        case CH19:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (13));
            break; 
        case CH20:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (12));
            break; 
        case CH21:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (11));
            break;  
        case CH22:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (10));
            break; 
        case CH23:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (9));
            break;   
        case CH24:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (8));
            break;  
        case CH25:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (7));
            break; 
        case CH26:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (6));
            break;     
        case CH27:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (5));
            break;                       
        case CH28:
            state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (4));
            break;    
        
        default:
            break;
        }
        //state_LED_Out_17_28 ^= (-state_LED[I2C_Expander_CH - (ch - MAX_NUMBER_OF_I2C_Channels)] ^ state_LED_Out_17_28) & (1 << (ch - MAX_NUMBER_OF_I2C_Channels));
        //state_LED_Out_17_28 ^= (-state_LED[ch] ^ state_LED_Out_17_28) & (1 << (ch - MAX_NUMBER_OF_I2C_Channels));
        // Parameter1: Status LED "ON" / "OFF"  ||   Parameter2: Status LEDs KO gesteuert
        //if (knx.paramByte(BIN_LEDdisplay) && knx.paramByte(BIN_LEDdisplayKOActivation) == 0 || status_Led_ON)
        if (true)
        {
            SERIAL_PORT.print("SET CH");
            SERIAL_PORT.println(state_LED_Out_17_28,BIN);
            pcf8575_LED_CH17_28.pcf8575_WriteALL(state_LED_Out_17_28);   //*************************************************************************************************************************
        }
    }
}